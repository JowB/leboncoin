import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AdService} from "../ad/ad.service";

@Component({
  selector: 'app-ad-view',
  templateUrl: './ad-view.component.html',
  styleUrls: ['./ad-view.component.scss']
})
export class AdViewComponent implements OnInit {

  private id:number;
  ad: {};
  constructor(private route: ActivatedRoute, private adService:AdService) { }

  ngOnInit() {
    this.id = Number(this.route.snapshot.params['id']);
    this.ad = this.adService.getAdById(this.id);
  }

}
