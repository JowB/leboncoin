import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/interval";
import {Subject} from "rxjs/Subject";

@Component({
  selector: 'app-exercice',
  templateUrl: './exercice.component.html',
  styleUrls: ['./exercice.component.scss']
})
export class ExerciceComponent implements OnInit, OnDestroy {

  compteur: any = 0;
  observable: any;
  subscription: any;
  isSubscribe = false;
  interval: any;

  constructor() {
  }

  ngOnInit() {
  }

  onCompteur() {
    this.isSubscribe = true;

    //          Observale.interval
    /* let observable = Observable.interval(1000);
    this.subscription = observable.subscribe((data)=>{this.compteur = data}, console.log, console.log);*/

    //          Observable.create
    /*this.observable = Observable.create((observer) => {
    let time = 0;
      this.interval = setInterval(() => {
        observer.next(time);
        time++;
      }, 1000);
      observer.complete()
    });
    this.subscription = this.observable.subscribe((data) => {this.compteur = data}, console.log, console.log);*/

    //          Subject
    let subject = new Subject<number>();

    this.subscription = subject.subscribe((data) => {this.compteur = data});

    let time = 0;
    this.interval = setInterval(() => {
      subject.next(time);
      time++;
    }, 1000);
  }

  offCompteur() {
    this.ngOnDestroy();
  }

  ngOnDestroy() {
    this.isSubscribe = false;
    this.subscription.unsubscribe();
    this.compteur = 0;
    clearInterval(this.interval);
  }
}
