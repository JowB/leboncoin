import {Component, DoCheck, OnInit, Input} from '@angular/core';
import {AdService} from "../ad/ad.service";
import {Status} from "../status";

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

  count: number = 0;

  constructor(private adService: AdService) { }

  ngOnInit() {
    this.adService.annoncesSubject.subscribe(
      (ads) => {
        this.count = ads.filter((ad) => ad.status == Status.Published).length;
      }
    );
  }
}
