import {Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {AdsComponent} from "./ads/ads.component";
import {AdViewComponent} from "./ad-view/ad-view.component";
import {Error404Component} from "./error-404/error-404.component";
import {AuthGuard} from "./auth.guard";
import {ExerciceComponent} from "./exercice/exercice.component";

export const appRoutes: Routes = [
  { path: '', component: AdsComponent, canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent},
  { path: 'ads', component: AdsComponent, canActivate: [AuthGuard]},
  { path: 'exo', component: ExerciceComponent },
  { path: 'ads/:id', component: AdViewComponent, canActivate: [AuthGuard]},
  { path: '404', component: Error404Component},
  { path: '**', redirectTo: '404'}
];
