import {Component, OnInit} from '@angular/core';
import {AdService} from "../ad/ad.service";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.scss']
})
export class AdsComponent implements OnInit {

  private ads: any;

  constructor(private AdService: AdService, private authService: AuthService) {
  }

  ngOnInit() {
    this.AdService.annoncesSubject.subscribe(
      (data) => this.ads = data,
    );
    this.AdService.getAds();
  }

  onPublish() {
    this.AdService.allPublish();
  }

  logout() {
    this.authService.logout();
  }
}
