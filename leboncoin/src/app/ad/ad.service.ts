import {Injectable} from '@angular/core';
import {Status} from "../status";
import {HttpClient} from "@angular/common/http";
import {Subject} from "rxjs/Subject";
import {forEach} from "@angular/router/src/utils/collection";

@Injectable()
export class AdService {

  annoncesSubject: Subject<any> = new Subject<any>();
  private annonces: any = [];

  constructor(private http: HttpClient) {
  }

  getAdById(id: number) {
    for (let annonce of this.annonces) {
      if (annonce.id == id) {
        return annonce;
      }
    }
  }

  emitAnnonces() {
    this.annoncesSubject.next(this.annonces);
  }

  getAds() {
    this.http.get<any>('https://leboncoin-7d69d.firebaseio.com/ads.json').subscribe(
      (data) => {
        let ads = [];
        for (let [id, ad] of Object.entries(data)) {
          ad.id = id;
          ads.push(ad);
        }
        console.log('ads', ads);
        this.annonces = ads;
        this.emitAnnonces();
      },
      (error) => console.log(error),
      () => console.log("finish")
    )
  }

  allPublish() {
    for (let i = 0; i < this.annonces.length; i++) {
      this.annonces[i].status = Status.Published;
    }
    this.emitAnnonces();
  }

  publish(index) {
    this.annonces[index].status = Status.Published;
    this.emitAnnonces();
  }

  offline(index) {
    this.annonces[index].status = Status.Offline;
    this.emitAnnonces();
  }

  draft(index) {
    this.annonces[index].status = Status.Draft;
    this.emitAnnonces();
  }
}
