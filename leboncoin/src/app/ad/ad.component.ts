import {Component, Input, OnInit} from '@angular/core';
import {Status} from "../status";
import {AdService} from "./ad.service";

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})
export class AdComponent implements OnInit {

  @Input() title: string;
  @Input() status: string;
  @Input() date: Date;
  @Input() price: number;
  @Input() index: number;
  @Input() id: number;
  Status = Status;

  constructor(private adService: AdService) {
  }

  getStatusColor() {
    switch (this.status) {
      case this.Status.Draft :
        return 'gray';
      case this.Status.Published :
        return 'green';
      case this.Status.Offline :
        return 'red';
    }
  }

  ngOnInit() {
  }

  draftOne() {
    this.adService.draft(this.index);
  }

  publishedOne() {
    this.adService.publish(this.index);
  }

  offlineOne() {
    this.adService.offline(this.index);
  }
}
