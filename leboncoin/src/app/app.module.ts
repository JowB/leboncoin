import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AdComponent } from './ad/ad.component';
import {FormsModule} from "@angular/forms";
import {AngularFontAwesomeModule} from "angular-font-awesome";
import {AdService} from "./ad/ad.service";
import { CounterComponent } from './counter/counter.component';
import { LoginComponent } from './login/login.component';
import {RouterModule} from "@angular/router";
import {appRoutes} from "./route";
import { AdsComponent } from './ads/ads.component';
import { NavComponent } from './nav/nav.component';
import { AdViewComponent } from './ad-view/ad-view.component';
import { Error404Component } from './error-404/error-404.component';
import {AuthService} from "./auth.service";
import {AuthGuard} from "./auth.guard";
import {HttpClientModule} from "@angular/common/http";
import { ExerciceComponent } from './exercice/exercice.component';


@NgModule({
  declarations: [
    AppComponent,
    AdComponent,
    CounterComponent,
    LoginComponent,
    AdsComponent,
    NavComponent,
    AdViewComponent,
    Error404Component,
    ExerciceComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    AngularFontAwesomeModule,
    HttpClientModule
  ],
  providers: [
    AdService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
